#include <PrintEx.h> //allows printf-style printout syntax

StreamEx mySerial = Serial; //added for printf-style printing

const bool debug = 0;           // enable serial debug

/* Pin Assignments - LED Indicators */
const int statusLED = 13;
const int LEDpanel1 =  9;       // pwm out for panel LED1 
const int LEDpanel2 =  10;      // pwm out for panel LED2 
const int LEDpanel3 =  11;      // pwm out for panel LED3 

const bool panelPWM = 1;        // disable pwm on the panel to use more dimmable lights

/* Pin Assignments - Light Outputs */
const int Light1 = 5;           // PWM output to MOSFET LED driver
const int Light2 = 6;           // PWM output to MOSFET LED driver

const int Sensor1 = 7;          // D in for PID sensor
const int Sensor2 = A0;         // A in for light sensor

const int Button1 = 2;          // D in for Bright+ switch
const int Button2 = 3;          // D in for Bright- switch
const int fader1Pot = A1;        // A in from 10k pot
const int fader1Max = 430;       // calibration for the map function
const int fader1Min = 170;       // calibration for the map function


// Timers
const unsigned long sensor1Timer = 1000;         // motion sensor interval
unsigned long sensor1Event;

bool motionTrigger = 0;                     // if we are in a motion trigger event or not
const unsigned long motionDuration = 60000; // time in millis to leave the light on
unsigned long motionEvent;                  // time when the event begins (motionTrigger goes high)


/* Twilight Sensor


*/
const unsigned long twilightTimer = 1000;    // twilight sensor interval
unsigned long twilightEvent;


int twilightValue = 0;           // the analog sensor value (0-255)
bool twilightState = 0;          // the current daylight sensor state (0/1)
const int twilightThreshold = 512;     // the analog value set to trigger change (1 - 254)
const int twilightMin = 100;     // calibration
const int twilightMax = 750;     // calibration

bool twilightBuffer[5];         // when the buffer is full, a change is detected
bool nightMode = 0;             // 1 when it is night time, night light is enabled


const unsigned long fader1Timer = 200;      // how often to poll the pot
unsigned long fader1Event;
int fader1Value;                            // value 0-255

// debug status line in the serial monitor
const unsigned long statusTimer = 1000;     // interval for update display 
unsigned long statusEvent = 0;              // stores millis() for periodically reporting sensor state


// UI Settings
const int dim[5] = {0, 25,70, 100, 255};   // off, the three preset fade states, maximum
int dimLevel = 0;               // for fading the main lights
int dimPrevious = 0;
int dimTarget = 128;            // initial brightness 50%
int panelDimLevel = 128;          // for fading the panel lights up and down
int panelDimPrevious = 0;

// button inputs & debounce
bool button1State = 0;          // "bright+" state after the debounce
bool b1reading;                 // debounce - the current reading from the button pin
bool b1previous = 0;            // debounce - the previous reading from the button pin

bool button2State = 0;          // "bright-" state after the debounce
bool b2reading;                 // debounce - the current reading from the button pin
bool b2previous = 0;            // debounce - the previous reading from the button pin

bool sensor1State = 0;          // the current motion sensor state (0, 1)
bool sensor1Previous = 0;       // the previous state (for change detection)
bool motionDetected = 0;        // motion event detected
int previousDimLevel = 0;       // the dim level before motion trigger

// for button debounce
unsigned long time = 0;                  // the last time the button pin was toggled
unsigned long debounce = 200;            // the debounce time, increase if multiple presses are triggered


/* Brightness and Fades */
// Main Lights
int light1Bright = 0;           // Brightness of the output 
int light1Target = 0;           // Based on conditions at the time of keypress
unsigned long light1Event = 0;  // timer for update
unsigned long light1Fade = 10;  // millisecond fade steps

int light2Bright = 0;           // Brightness of the output 
int light2Target = 0;           // Based on conditions at the time of keypress
unsigned long light2Event = 0;  // timer for update
unsigned long light2Fade = 10;  // millisecond fade steps

// Panel LEDs
unsigned long panelFade = 5;    // fade speed for the panel leds (lower is faster)

bool panel1State = 0;
int panel1Bright = 0;           // Brightness of the output 
int panel1Target = 0;           // Target brightness for fade
unsigned long panel1Event = 0;  // timer for fade

bool panel2State = 0;
int panel2Bright = 0;           // Brightness of the output
int panel2Target = 0;           // Target brightness for fade
unsigned long panel2Event = 0;  // timer for fade

bool panel3State = 0;
int panel3Bright = 0;           // Brightness of the output
int panel3Target = 0;           // Target brightness for fade
unsigned long panel3Event = 0;  // timer for fade



void setup() {
    // LED pin definitions
    pinMode(statusLED, OUTPUT);
    pinMode(LEDpanel1, OUTPUT);
    pinMode(LEDpanel2, OUTPUT);
    pinMode(LEDpanel3, OUTPUT);

    // Light pin definitions
    pinMode(Light1, OUTPUT);
    pinMode(Light2, OUTPUT);
    
    // button pins and interrupts

    pinMode(Button1, INPUT_PULLUP);
    pinMode(Button2, INPUT_PULLUP);
    


    button1State = digitalRead(Button1);
    mySerial.printf("Initial + button state is %s\n", button1State == LOW ? "LOW" : "HIGH");
    attachInterrupt(digitalPinToInterrupt(Button1), button1Interrupt, CHANGE);

    button2State = digitalRead(Button2);
    mySerial.printf("Initial - button state is %s\n", button2State == LOW ? "LOW" : "HIGH");
    attachInterrupt(digitalPinToInterrupt(Button2), button2Interrupt, CHANGE);



    // sensor pin definitions
    pinMode(Sensor1, INPUT_PULLUP);     // digital signal from PIR sensor
    pinMode(Sensor2, INPUT);            // analog LDR daylight sensor
    
    Serial.begin(115200);
    Serial.println("Setup is done!");
    
    /* Test the panel */
    digitalWrite(statusLED, 1);
    delay(100);
    digitalWrite(statusLED, 0);



    // panelDimTarget = read_fader(fader1Pot, fader1Min, fader1Max); 
    mySerial.printf("Panel Brightness: %i\n", panelDimLevel);
    

    // Blink the LEDS
    blink_LED(LEDpanel1, 3, 100, 100, panelDimLevel, panel1State);
    blink_LED(LEDpanel2, 3, 100, 100, panelDimLevel, panel2State);
    blink_LED(LEDpanel3, 3, 100, 100, panelDimLevel, panel3State);

    // initial values before entering the loop
    sensor1State = digitalRead(Sensor1);
    Serial.print("Motion Sensor Value: ");
    Serial.println(sensor1State);

    twilightValue = analogRead(Sensor2);
    Serial.print("Twilight Sensor Value: ");
    Serial.println(twilightValue);

    // get_twilight();
    
    // show_debug();
}




// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

volatile bool b1Interrupt = false;     // indicates whether MPU interrupt pin has gone high
void button1Interrupt() 
{
    b1Interrupt = true;
}


volatile bool b2Interrupt = false;     // indicates whether MPU interrupt pin has gone high
void button2Interrupt() 
{
    b2Interrupt = true;
}

void blink_LED(int LED, int blinks, int onTime, int offTime, int bright, bool state){
    delay(offTime);
    for (byte i=0; i < blinks; i++){
        analogWrite(LED, bright);
        // Serial.print(" * ");
        delay(onTime);
        analogWrite(LED, 0);
        delay(offTime);
    }
    // reset the LED to previous state and brightness
    analogWrite(LED, state ? bright: 0);
    
}

void get_twilight(){

    twilightValue = analogRead(Sensor2);    // high value is darker

    if (twilightValue > twilightThreshold){
        twilightState = 1;
    }
    else twilightState = 0;


    // push a new value to the array, check if all values are equal, check for a change
    for (int i=0;i<=4;i++){
        twilightBuffer[i] = twilightBuffer[i+1]; //move all element to the right except last one
    }
    twilightBuffer[5] = twilightState; //assign current value to last element
    // if the buffer is all equal, set nightmode to that value
    for(int i=0;i<5;i++){
        if (twilightBuffer[i] != twilightBuffer[0]) break;
        
        if (twilightBuffer[0] != nightMode){
            nightMode = twilightBuffer[0];
            mySerial.printf(" Daylight Sensor changed to %s\n", nightMode == 1 ? "Night" : "Day");
            twilight();     // sets all of the day/night settings after a transition
        }
    }
}


void twilight(){    // sets the panel brightness and motion settings, etc.

    // mySerial.printf("Changes to  %s ", nightMode ? "Night" : "Day  ");
    if (nightMode == 0){
        // Daytime
        panelDimLevel = dim[4];
    }
    else {
        panelDimLevel = dim[2];
    }
    // previousNightMode = nightMode;
    // do_fade(LEDpanel1, panelDimLevel, panelDimTarget, 10);   // slow fade
    // // do_fade(LEDpanel2, panelDimLevel, panelDimTarget, 10);   // slow fade
    // // do_fade(LEDpanel3, panelDimLevel, panelDimTarget, 10);   // slow fade
    // panelDimPrevious = panelDimLevel;
    // panelDimLevel = panelDimTarget;
    panel1Target = panelDimLevel;

}

    


void get_motion(){

    if(sensor1State == 0 && sensor1Previous == 1){    
        // Serial.println(" Motion Sensor triggered ");
        motionDetected = 1; 
    }
    else if (sensor1State == 0 && sensor1Previous == 0){
        // Serial.println(" No Motion ");
        motionDetected = 0;
    }


}

int read_fader(int potPin, int min, int max){
    // read the fader pot to set  brightness
    // return a value 0-255
    int fader;
    // map range to useful min & max (1,255) 
    fader = map(analogRead(potPin), min, max, 1, 255);
    if (fader < 0) fader = 0;
    if (fader > 255) fader = 255;

    return fader;
}


void show_debug(){
    // mySerial.printf("Light1 Bright: %i   Target: %i   Fade: %i\n", light1Bright, light1Target, light1Fade);
    mySerial.printf("Twilight State: %s  Night Mode: %s  Sensor: %i/%i,  red LED: %s  ", 
                        twilightState ? "Night" : "Day  ", 
                        nightMode ? "Night" : "Day  ", 
                        twilightValue, twilightThreshold, 
                        panel1State ? "On" : "Off");

    mySerial.printf(" %s - %s - %s - %s - %s\n", 
        twilightBuffer[0] ? "N" : "D", 
        twilightBuffer[1] ? "N" : "D", 
        twilightBuffer[2] ? "N" : "D", 
        twilightBuffer[3] ? "N" : "D", 
        twilightBuffer[4] ? "N" : "D");

    // Serial.print("* ");
    // Serial.print("keypress :");
    // Serial.print(keypress);
    // Serial.print(" - ");
    // Serial.print("Motion :");
    // Serial.print(sensor1State);
    // Serial.print(" - ");
    // Serial.print("Twilight :");
    // Serial.print(twilightValue);
    // Serial.print(" - ");
    // Serial.print("Display Fader : ");
    // Serial.print(panelDimLevel);
    // Serial.print(" - ");
    // Serial.print("Twilight Sensor : ");
    
    // for (int i = 0; i <= 4; i++){
    //     if (dayNightBuffer[i] == 0){
    //             Serial.print("D");
    //     }
    //     else{
    //         Serial.print("N");
    //     }


    //     // switch(dayNightBuffer[i]){
    //     //     case 1:
    //     //         Serial.print("D");
    //     //     case 0:
    //     //         Serial.print("N");

    //     // }

    //     // Serial.print(dayNightBuffer[i]);
    //     // dayNightBuffer[i] = "";
    // }
    // Serial.print(" - Night Mode: ");
    // Serial.print(nightMode);


    // Serial.println(" *");









    
}

void upButton(){

    if (dimLevel == 4) {
        Serial.println("Already at maximum");
        blink_LED(LEDpanel1, 5, 100, 100, panelDimLevel, panel1State);
    }

    else { 
        dimLevel++;
        Serial.print("Dim Level : ");
        Serial.println(dimLevel);
        light1Target = dim[dimLevel];
    }
    blink_LED(LEDpanel2, dimLevel + 1, 250, 100, panelDimLevel, panel2State);

}

void downButton(){

    if (dimLevel == 0) {
        Serial.println("Already at minimum");
        blink_LED(LEDpanel1, 5, 100, 100, panelDimLevel, panel1State);
    }

    else { 
        dimLevel--;
        Serial.print("Dim Level : ");
        Serial.println(dimLevel);
        light1Target = dim[dimLevel];
    }
    blink_LED(LEDpanel2, dimLevel + 1, 250, 100, panelDimLevel, panel2State);
}





void loop(){
//     blink_LED(statusLED, 3, 200, 400);

    if (b1Interrupt)
    {
        delay(debounce);        //ignore bounces for 200mSec
        b1Interrupt = false;
        blink_LED(LEDpanel3, 1, 50, 50, panelDimLevel, panel3State);
        upButton();
    }

    if (b2Interrupt)
    {
        delay(debounce);        //ignore bounces for 200mSec
        b2Interrupt = false;
        blink_LED(LEDpanel3, 2, 50, 50, panelDimLevel, panel3State);
        downButton();
    }


    digitalWrite(statusLED, 1);       //waiting for someting to happen
    

    // update all of the outputs
    // panel1Previous = panel1State;
    // panel2Previous = panel2State;
    // panel3Previous = panel3State;

    // when nightMode begins, the red LED is illuminated
    if (nightMode != panel1State ){
        panel1State = nightMode;
        panel1Target = nightMode ? dim[4] : dim[0];
    }


    /*
        CONTINUE FADES
    */
    // main light fade is underway
    if (light1Bright != light1Target){
        if (millis() > light1Event + light1Fade){
            if (light1Target > light1Bright) {
                light1Bright ++;
                if (debug) Serial.print("+");
            }
            else {
                light1Bright --;
                if (debug) Serial.print("-");

            }
            analogWrite(Light1, light1Bright);

        }
    }

    if (panel1Bright != panel1Target){
        if (millis() > panel1Event + panelFade){
            if (panel1Target > panel1Bright) {
                panel1Bright ++;
                if (debug) Serial.print("+");
            }
            else {
                panel1Bright --;
                if (debug) Serial.print("-");

            }
            analogWrite(LEDpanel1, panel1Bright);

        }




    //kill the LED if PWM gets to zero
    // if(light1Bright == 0){
    //     digitalWrite(Light1, 0);
    // }
        
    }

    
    
    
    
    
    
    // digitalWrite(LEDpanel3, 0);
    // Serial.println("Waiting for input.... ");

    // TODO: calibration 
    // while (keypress == 0){
    //     panelDimLevel = analogRead(faderPot); // 160 - 330
    //     Serial.print("Raw Pot Value: ");
    //     Serial.print(panelDimLevel);
    //     panelDimLevel = read_fader(faderPot, faderMin, faderMax);
    //     Serial.print("    Calibrated Dim Level: ");
    //     Serial.println (panelDimLevel);

    // }


    /* Update sensors and faders*/
    if(millis() > fader1Event + fader1Timer){
        if (debug) Serial.print("@");
        fader1Event = millis();
        fader1Value = read_fader(fader1Pot, fader1Min, fader1Max);
    }

    if (millis() > sensor1Event + sensor1Timer){
        if (debug) Serial.print("#");
        sensor1Event = millis();
        get_motion();
    }

    if (millis() > twilightEvent + twilightTimer){
        if (debug) Serial.print("$");
        twilightEvent = millis();
        get_twilight();
        if (debug) Serial.print("%");

    }



    // Serial.println("");


    // display a status line every x milliseconds
    if (millis() > statusEvent + statusTimer){
        statusEvent = millis();
        // Serial.print("=");
        show_debug();

    }

    
    // store panel values (on/off) and fade to the day/night brightness



    // Clean up any modes that may be active - motion, twilight, etc.
    // if (motionTrigger){
    //     // is it time to end a motion event?
    //     if (millis() >  motionEvent + motionDuration && motionTrigger && sensor1State == 0){
    //         // Serial.println  ("End The Fade");
    //         // dimLevel = previousDimLevel;
    //         // light1Target = dim[dimLevel];
    //         motionTrigger = 0;
    //         // do the fade
    //     }
    // }
    


    /* DEPRECATED - process any keypress actions*/
    // if (keypress > 0){
    //     digitalWrite(statusLED, 0);      //key pressed - about to initiate a fade.But which kind?
    //     blink_LED(LEDpanel3, keypress, 100, 100);
    // }

    // process a keypress
    // switch(keypress){
        // case 1:
        //     if (dimLevel == 4) Serial.println("Already at maximum");
        //     else dimLevel++;
        //     Serial.print("Dim Level : ");
        //     Serial.println(dimLevel);
        //     blink_LED(LEDpanel2, dimLevel + 1, 250, 100);
        //     light1Target = dim[dimLevel];

        //     break;
        
        // case 2:
        //     if (dimLevel == 0) Serial.println("already off");
        //     else dimLevel--; 
        //     Serial.print("Dim Level : ");
        //     Serial.println(dimLevel);
        //     blink_LED(LEDpanel2, dimLevel + 1, 250, 100);
        //     light1Target = dim[dimLevel];

        //     break;
    //     case 3:
    //         // The motion sensor triggered.
    //         // new motion trigger event
    //         motionTrigger = 1;
    //         motionEvent = millis();
    //         Serial.print("New Motion Trigger: end time = ");
    //         Serial.println(motionEvent + motionDuration);
    //         Serial.print("Setting dim level to ");
    //         previousDimLevel = dimLevel;
    //         dimLevel++;
    //         Serial.println(dimLevel);
    //         Serial.print("and begin fade to ");
    //         light1Target = dim[dimLevel];
    //         Serial.println(light1Target);
            
    //         digitalWrite(LEDpanel1, 1);
    //         digitalWrite(LEDpanel2, 1);
    //         digitalWrite(LEDpanel3, 0);
            
    //         break;
    // }   


        
    // success = do_fade();
    // Serial.print("Fade status: ");
    // Serial.println(success);
}
